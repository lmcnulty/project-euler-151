#!/usr/bin/env python3

import sys

markup = ""

with open(sys.argv[1], 'r') as f:
	last_line_code = False
	last_line_code_header = False
	start = False
	for line in f:
		if '---' in line:
			start = True
		if not start:
			continue
		if len(line) < 2 and last_line_code_header:
			continue
		if line[0] == "#" and line[1] == " ":
			if last_line_code:
				markup += "```\n"
			markup += line[2:-1] + "\n"
			last_line_code = False
			last_line_code_header = False
		else:
			if not last_line_code:
				markup += "```python\n"
				last_line_code_header = True

			if not (
				last_line_code_header and 
				(line.isspace() or len(line) < 1)
			):
				markup += line
			
			if last_line_code:
				last_line_code_header = False

			last_line_code = True

if last_line_code: markup += "```"
print(markup.replace("```python\n```", ""))


